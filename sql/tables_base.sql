#################################
## Příkazy pro odstranění tabulek
#################################

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS vozidlo;
DROP TABLE IF EXISTS kradene_vozidlo;
DROP TABLE IF EXISTS technicka_kontrola;
DROP TABLE IF EXISTS spz;
DROP TABLE IF EXISTS ridic;
DROP TABLE IF EXISTS ridicsky_prukaz;
DROP TABLE IF EXISTS prestupek;
DROP TABLE IF EXISTS seznam_prestupku;

SET foreign_key_checks = 1;

################################
## Příkazy pro vytvoření tabulek
################################

CREATE TABLE `seznam_prestupku` (
    `id` INT NOT NULL,
    `popis` TEXT NOT NULL,
    `body` INT NOT NULL,
    `pokuta` DECIMAL(10,5) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE `ridic` (
    `id` INT NOT NULL,
    `jmeno` VARCHAR(255) NOT NULL,
    `prijmeni` VARCHAR(255) NOT NULL,
    `rodne_cislo` VARCHAR(32) NOT NULL,
    `narozen` DATE NOT NULL,
    `zakaz_rizeni` DATE NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE `ridicsky_prukaz` (
    `id` INT NOT NULL,
    `id_ridic` INT NOT NULL,
    `cislo` VARCHAR(32) NOT NULL,
    `vydano` DATE NOT NULL,
    `platnost` DATE NOT NULL,
    PRIMARY KEY (`id`),
    KEY `id_ridic` (`id_ridic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE `prestupek` (
    `id` INT NOT NULL,
    `id_ridic` INT NOT NULL,
    `id_seznam_prestupku` INT NOT NULL,
    `datum` DATE NOT NULL,
    `misto` TEXT NOT NULL,
    `body` INT NOT NULL,
    PRIMARY KEY (`id`),
    KEY `id_ridic` (`id_ridic`),
    KEY `id_seznam_prestupku` (`id_seznam_prestupku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE `spz` (
	`id` INT NOT NULL,
	`kod` VARCHAR(50) NOT NULL,
	`kraj` VARCHAR(255) NOT NULL,
	`prideleno` DATE NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE `vozidlo`(
	`id` INT NOT NULL,
	`id_ridic` INT NOT NULL,
	`id_spz` INT,
	`vyrobni_cislo` INT NOT NULL,
	`znacka` VARCHAR(255) NOT NULL,
	`model` VARCHAR(255) NOT NULL,
	`rok_vyroby` DATE NOT NULL,
	`palivo` VARCHAR(255) NOT NULL,
	`vaha` INT NOT NULL,
	`max_pocet_cestujicich` INT NOT NULL,
	`max_rychlost` INT NOT NULL,
    PRIMARY KEY (`id`),
    KEY `id_ridic` (`id_ridic`),
    KEY `id_spz` (`id_spz`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE `technicka_kontrola` (
	`id` INT NOT NULL,
	`id_vozidlo` INT NOT NULL,
	`stav` TEXT NOT NULL,
	`provedeno` DATE NOT NULL,
	`platnost` DATE NULL,
    PRIMARY KEY (`id`),
    KEY `id_vozidlo` (`id_vozidlo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

CREATE TABLE `kradene_vozidlo` (
	`id` INT NOT NULL,
	`id_vozidlo` INT NOT NULL,
	`datum` DATE NOT NULL,
	`misto` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `id_vozidlo` (`id_vozidlo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

###############################
## Příkazy pro vytvoření relací
###############################

ALTER TABLE `ridicsky_prukaz`
    ADD CONSTRAINT `ridicsky_prukaz__ridic` FOREIGN KEY (`id_ridic`) REFERENCES `ridic`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `prestupek`
    ADD CONSTRAINT `prestupek__ridic` FOREIGN KEY (`id_ridic`) REFERENCES `ridic`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
    ADD CONSTRAINT `prestupek__seznam_prestupku` FOREIGN KEY (`id_seznam_prestupku`) REFERENCES `seznam_prestupku`(`id`) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE `vozidlo`
    ADD CONSTRAINT `vozidlo__ridic` FOREIGN KEY (`id_ridic`) REFERENCES `ridic`(`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
    ADD CONSTRAINT `vozidlo__spz` FOREIGN KEY (`id_spz`) REFERENCES `spz`(`id`) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE `technicka_kontrola`
    ADD CONSTRAINT `technicka_kontrola__vozidlo` FOREIGN KEY (`id_vozidlo`) REFERENCES `vozidlo`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `kradene_vozidlo`
    ADD CONSTRAINT `kradene_vozidlo__vozidlo` FOREIGN KEY (`id_vozidlo`) REFERENCES `vozidlo`(`id`) ON UPDATE CASCADE ON DELETE CASCADE;
