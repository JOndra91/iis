#####################################
## Příkazy pro naplnění databáze daty
#####################################

INSERT INTO `seznam_prestupku` VALUES (1, 'Překročení rychlosti', 4, 2500);
INSERT INTO `seznam_prestupku` VALUES (2, 'Jízda na červenou', 2, 1200);
INSERT INTO `seznam_prestupku` VALUES (3, 'Řízení vlivem omamných látek', 5, 5000);
INSERT INTO `seznam_prestupku` VALUES (4, 'Telefonování za jízdy', 1, 4000);
INSERT INTO `seznam_prestupku` VALUES (5, 'Řízení bez dokladů', 2, 3000);

INSERT INTO `ridic` VALUES (1, 'Tomáš', 'Šimíček', '910101/1000', '1991-01-01', 0);
INSERT INTO `ridic` VALUES (2, 'Ondřej', 'Janošík', '910202/2000', '1991-02-02', 0);
INSERT INTO `ridic` VALUES (3, 'Jakub', 'Skrzeczek', '910303/3000', '1991-03-03', 1);
INSERT INTO `ridic` VALUES (4, 'Tomáš', 'Porwolik', '910404/4000', '1991-04-04', 0);
INSERT INTO `ridic` VALUES (5, 'Tomáš', 'Mihulka', '910505/5000', '1991-05-05', 0);

INSERT INTO `prestupek` VALUES (1, 3, 1, '2012-12-21', 'Ostrava', 5);
INSERT INTO `prestupek` VALUES (2, 3, 3, '2012-12-22', 'Praha', 4);
INSERT INTO `prestupek` VALUES (3, 4, 2, '2012-12-23', 'Brno', 2);
INSERT INTO `prestupek` VALUES (4, 5, 5, '2012-12-24', 'Olomouc', 2);

INSERT INTO `ridicsky_prukaz` VALUES (1, 1, '201301010001', '2013-01-01', '2017-01-01');
INSERT INTO `ridicsky_prukaz` VALUES (2, 2, '201302010002', '2013-02-01', '2017-02-01');
INSERT INTO `ridicsky_prukaz` VALUES (3, 3, '201303010001', '2013-03-01', '2017-03-01');
INSERT INTO `ridicsky_prukaz` VALUES (4, 4, '201304010001', '2013-04-01', '2017-04-01');
INSERT INTO `ridicsky_prukaz` VALUES (5, 5, '201305010001', '2013-05-01', '2017-05-01');

INSERT INTO `spz` VALUES (1, '4A23001', 'Středočeský', '2012-12-12');
INSERT INTO `spz` VALUES (2, '4A23002', 'Středočeský', '2012-11-12');
INSERT INTO `spz` VALUES (3, '4A23003', 'Středočeský', '2012-10-12');
INSERT INTO `spz` VALUES (4, '4A23004', 'Středočeský', '2012-09-12');
INSERT INTO `spz` VALUES (5, '4A23005', 'Středočeský', '2012-08-12');
INSERT INTO `spz` VALUES (6, '1T31234', 'Moravskoslezský', '2013-04-06');
INSERT INTO `spz` VALUES (7, '1T31235', 'Moravskoslezský', '2013-04-06');
INSERT INTO `spz` VALUES (8, '1T31236', 'Moravskoslezský', '2013-04-06');
INSERT INTO `spz` VALUES (9, '1T31237', 'Moravskoslezský', '2013-04-06');
INSERT INTO `spz` VALUES (10, '1T31238', 'Moravskoslezský', '2013-04-06');

INSERT INTO `vozidlo` VALUES (1, 1, 1, 1111111, 'Škoda', 'Yeti', '2012', 'benzín', 1000, 3, 150);
INSERT INTO `vozidlo` VALUES (2, 2, 2, 2222222, 'Škoda', 'SuperB', '2012', 'LPG', 1000, 5, 250);
INSERT INTO `vozidlo` VALUES (3, 4, 3, 4444444, 'Audi', 'R5', '2013', 'benzín', 1500, 5, 300);
INSERT INTO `vozidlo` VALUES (4, 4, 8, 6881105, 'Velorex', 'OS#KAR', '1953', 'benzín', 120, 2, 60);

INSERT INTO `kradene_vozidlo` VALUES (1, 2,  '2013-01-01', 'Ostrava');
INSERT INTO `kradene_vozidlo` VALUES (2, 2,  '2013-02-02', 'Praha');

INSERT INTO `technicka_kontrola` VALUES (1, 2, 'V pořádku', '2013-01-01', '2015-01-01');
INSERT INTO `technicka_kontrola` VALUES (2, 1, 'V pořádku', '2013-01-02', '2015-01-02');
INSERT INTO `technicka_kontrola` VALUES (3, 3, 'V pořádku', '2014-12-10', '2016-12-10');
INSERT INTO `technicka_kontrola` VALUES (4, 2, 'V pořádku', '2015-01-27', '2017-01-27');
INSERT INTO `technicka_kontrola` VALUES (5, 1, 'Zkorodovaná nádrž', '2014-12-10', NULL);
