#################################
## Příkazy pro odstranění tabulek
#################################

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS users;

SET foreign_key_checks = 1;

################################
## Příkazy pro vytvoření tabulek
################################

CREATE TABLE `users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(50) NOT NULL,
    `password` CHAR(60) NOT NULL,
    `role` VARCHAR(32) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;