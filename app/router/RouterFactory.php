<?php

use Nette\Application\Routers\RouteList,
    Nette\Application\Routers\Route,
    Nette\Application\Routers\SimpleRouter;

/**
 * Router factory.
 */
class RouterFactory
{

    /**
     * @return Nette\Application\IRouter
     */
    public function createRouter()
    {
        $router   = new RouteList();

        $iisRouter = new RouteList('IIS');
        $iisRouter[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');

        $router[] = $iisRouter;

        return $router;
    }

}
