<?php

namespace IISModule;

use Nette\Application\UI;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{

    /**
     * @service iis.user
     * @var UserManager
     */
    protected $userManager;

    /**
     * @service authenticator
     * @var Authenticator
     */
    protected $authenticator;

    /**
     * Sign-in form factory.
     * @return Form
     */
    protected function createComponentSignInForm()
    {
        $form = new UI\Form;

        $form->getElementPrototype()->addAttributes(array('class' => 'signInForm'));

        $form->addText('username', 'Uživatelské jméno:')
                ->setRequired('Please enter your username.');

        $form->addPassword('password', 'Heslo:')
                ->setRequired('Please enter your password.');

        $form->addCheckbox('remember', 'Zapamatovat si přihlášení');

        $form->addSubmit('send', 'Přihlásit');

        // call method signInFormSucceeded() on success
        $form->onSuccess[] = $this->signInFormSucceeded;
        return $form;
    }

    public function signInFormSucceeded($form)
    {
        $values = $form->getValues();

        if ($values->remember)
        {
            $this->getUser()->setExpiration('14 days', FALSE);
        }
        else
        {
            $this->getUser()->setExpiration('20 minutes', TRUE);
        }

        try
        {
            $this->getUser()->login($values->username, $values->password);
            $this->redirect('Homepage:');
        }
        catch (AuthenticationException $e)
        {
            $form->addError($e->getMessage());
        }
    }

    public function actionIn()
    {
        $parameters = $this->getContext()->getParameters();

        if (isset($parameters['defaultUsers']))
        {

            foreach ($parameters['defaultUsers'] as $username => $user)
            {
                $userEntity = $this->userManager->findByUsername($username);
                if (!$userEntity)
                {
                    $userEntity = $this->userManager->createEntity();
                    /* @var $userEntity User */
                    $userEntity->setUsername($username);
                    $userEntity->setName($user['name']);
                    $userEntity->setSurname($user['surname']);
                    $userEntity->setRole($user['role']);
                    $userEntity->setPassword($this->authenticator->calculateHash($user['password']));

                    $this->userManager->persist($userEntity);
                }
            }

            $this->userManager->flush();
        }
    }

    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('Byl jste odhlášen');
        $this->redirect('in');
    }

}
