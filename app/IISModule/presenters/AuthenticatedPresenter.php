<?php

namespace IISModule;

/**
 * AuthenticatedPresenter
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class AuthenticatedPresenter extends BasePresenter
{

    protected function startup()
    {
        parent::startup();

        if (!$this->user->loggedIn)
        {
            if ($this->user->getLogoutReason() == \Nette\Security\User::INACTIVITY)
            {
                $this->flashMessage('Proběhlo automatické odhlášení');
            }
            $this->redirect('Sign:in');
        }
    }

    /**
     * Check if user has required roles
     * @param ArrayAccess $roles
     */
    protected function requireRole($roles, $error = null)
    {
        if (!is_array($roles))
        {
            $roles = array($roles);
        }

        foreach ($roles as $role)
        {
            if ($this->user->isInRole($role))
            {
                return true;
            }
        }

        $this->error($error ? : 'K požadované operaci nemáte oprávnění', 403);
    }

}
