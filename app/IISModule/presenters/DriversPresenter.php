<?php

namespace IISModule;

use Nette\Application\UI\Form;
use InvalidArgumentException;
use Nette\DateTime;
use UnexpectedValueException;

/**
 * DriversPresenter
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class DriversPresenter extends AuthenticatedPresenter
{

    /**
     * @service iis.driver
     * @var DriverManager
     */
    protected $driverManager;

    /**
     * @service iis.offenseDefinition
     * @var OffenseDefinitionManager
     */
    protected $offenseDefinitionManager;

    /**
     * @service iis.driverLicense
     * @var DriverLicenseManager
     */
    protected $driverLicenseManager;

    /**
     * @service iis.offense
     * @var OffenseManager
     */
    protected $offenseManager;

    /**
     * @var Driver
     */
    protected $driverEntity;

     /**
     *
     * @var DriverLicense
     */
    protected $offenseEntity;

    protected function startup()
    {
        parent::startup();

        $this->requireRole(array(User::OFFICER, User::OFFICIAL));
    }

    public function actionEdit($id)
    {
        $driver = $this->driverManager->findById($id);

        if (!$driver)
        {
            $this->flashMessage('Řidič nebyl nalezen', 'error');
            $this->redirect('Drivers:');
        }

        $this->driverEntity = $driver;
        $form               = $this->getComponent('driverEditForm');
        /* @var $form DriverForm */

        $form->addIdRule($this->driverEntity->getId());

        $values                      = $driver->toArray();
        $values['birthDate']         = $values['birthDate']->format('Y-m-d');
        $values['drivingRestricted'] = $values['drivingRestricted']->format('Y-m-d');
        if ($form->isSubmitted())
        {
            $values = array_merge($values, $form->getValues(true));
        }


        $form->setValues($values);
    }

    public function actionAddOffense($id)
    {
        $this->requireRole(User::OFFICER);

        $driver = $this->driverManager->findById($id);

        if (!$driver)
        {
            $this->flashMessage('Řidič nebyl nalezen', 'error');
            $this->redirect('Drivers:');
        }

        $this->driverEntity = $driver;
        $offenseForm        = $this->getComponent('offenseForm');
        /* @var $offenseForm OffenseForm */

        $offenseForm->setValues(array('driver' => $id));
    }

    public function actionAddLicense($id)
    {
        $this->requireRole(User::OFFICIAL);

        $driver = $this->driverManager->findById($id);

        if (!$driver)
        {
            $this->flashMessage('Řidič nebyl nalezen', 'error');
            $this->redirect('Drivers:');
        }

        $this->driverEntity = $driver;
        $licenseForm        = $this->getComponent('licenseForm');
        /* @var $licenseForm OffenseForm */

        if (!$licenseForm->isSubmitted())
        {
            $values = array(
                'driver' => $id,
                'code'   => $this->generateLicenseCode()
            );
            $licenseForm->setValues($values);
        }
    }

    public function actionEditOffense($id)
    {
        $this->requireRole(User::OFFICER);

        $this->offenseEntity = $this->offenseManager->findById($id);

        if (!$this->offenseEntity)
        {
            $this->flashMessage('Přestupek nebyl nalezen', 'error');
            $this->redirect('Drivers:');
        }

        $offenseForm = $this->getComponent('offenseForm');
        /* @var $theftForm TheftForm */

        $values = array_merge($this->offenseEntity->toArray(), array('driver' => $this->offenseEntity->getDriver()->getId()));

        $values['date'] = $values['date']->format('Y-m-d');

        if ($offenseForm->isSubmitted())
        {
            $values = array_merge($values, $offenseForm->getValues(true));
        }

        $offenseForm->setValues($values);
    }


    protected function createComponentDriverAddForm($name)
    {
        $form = new DriverForm($this, $name);

        $form->addIdRule();

        $form->onSuccess[] = array($this, 'onDriverFormSuccess');

        return $form;
    }

    protected function createComponentDriverEditForm($name)
    {
        $form = new DriverForm($this, $name);

        $form->onSuccess[] = array($this, 'onDriverFormSuccess');

        return $form;
    }

    protected function createComponentDriverGrid($name)
    {
        $grid = new DriverGrid($this, $name);

        $grid->disableFeaturesByUser($this->user);

        return $grid;
    }

    protected function createComponentOffenseForm($name)
    {
        $form = new OffenseForm($this, $name);

        $offenseEntities = $this->offenseDefinitionManager->findBy(array('enabled' => '1'), array('name' => 'asc'));

        $offenses = array();
        foreach ($offenseEntities as $offense)
        {
            /* @var $offense OffenseDefinition */
            $offenses[$offense->getId()] = $offense->getName();
        }

        $form->setOffenseItems($offenses);

        $form->onSuccess[] = array($this, 'onOffenseFormSuccess');

        return $form;
    }

    protected function createComponentLicenseForm($name)
    {
        $form = new LicenseForm($this, $name);

        $parameters = $this->getContext()->getParameters();

        $categoriesField = $form->getCategoriesField();
        $categoriesField->setItems($parameters['licenseCategories'], false);
        $categoriesField->setAttribute('size', count($parameters['licenseCategories']));

        $form->onSuccess[] = array($this, 'onLicenseFormSuccess');

        return $form;
    }

    public function onDriverFormSuccess(Form $form)
    {
        $values = $form->getValues();

        $values->offsetUnset('id');
        $values['birthDate']         = \Nette\DateTime::from($values['birthDate']);
        $values['drivingRestricted'] = \Nette\DateTime::from($values['drivingRestricted'] ? : '0000-00-00');

        $driver = $this->driverEntity;

        if (!$driver)
        {
            $driver = $this->driverManager->createEntity();
        }

        $driver->fromArray($values);

        try
        {
            $this->driverManager->persist($driver, true);
            $this->flashMessage('Údaje byly uloženy', 'success');
            $this->redirect('Drivers:edit', array('id' => $driver->getId()));
        }
        catch (InvalidArgumentException $e)
        {
            $this->flashMessage('Nepodařilo se vytvořit nového řidiče', 'error');
        }
    }

    public function onOffenseFormSuccess(Form $form)
    {
        $values = $form->getValues();

        $offenseDefinition = $this->offenseDefinitionManager->findById($values['offense']);

        if (!$offenseDefinition)
        {
            $this->flashMessage('Přestupek nebyl nalezen', 'error');
            return;
        }

        if($this->offenseEntity)
        {
            $offense=$this->offenseEntity;
        }
        else
        {
            $offense = $this->offenseManager->createEntityFromDefinition($offenseDefinition);
        }        
        /* @var $offense Offense */

        $offense->setDate(\Nette\DateTime::from($values['date']));
        $offense->setPlace($values['place']);

        if(!$this->offenseEntity)
        {
            $offense->setDriver($this->driverEntity);
            $this->driverEntity->addOffense($offense);
        }

        

        try
        {
            $this->offenseManager->persist($offense, true);
            $this->flashMessage('Přestupek byl zaznamenán', 'success');
            $this->redirect('Drivers:');
        }
        catch (InvalidArgumentException $e)
        {
            $this->flashMessage('Nepodařilo se zaznamenat přestupek', 'error');
        }
    }

    public function onLicenseFormSuccess(Form $form)
    {
        $values = $form->getValues();

        $driverLicense = $this->driverLicenseManager->createEntity();
        /* @var $driverLicense DriverLicense */

        $today = new DateTime();

        $driverLicense->setCode($values['code']);
        $driverLicense->setCategories($values['categories']);
        $driverLicense->setIssueDate($today);
        $driverLicense->setValidUntil($today->modifyClone('+10 years'));

        $driverLicense->setDriver($this->driverEntity);
        $this->driverEntity->addDriverLicense($driverLicense);

        try
        {
            $this->driverLicenseManager->persist($driverLicense, true);
            $this->flashMessage('Řidičský průkaz byl vyřízen', 'success');
            $this->redirect('Drivers:');
        }
        catch (UnexpectedValueException $e)
        {
            $this->flashMessage('Nepodařilo se vyřídit řidičský průkaz', 'error');
        }
    }

    protected function generateLicenseCode()
    {
        $a = ord('A');
        $z = ord('Z');


        while (true)
        {
            $code = "";

            $code .= chr(rand($a, $z));
            $code .= chr(rand($a, $z));

            $code .= " " . rand(100000, 999999);

            if (!$this->driverLicenseManager->licenseWithCodeExists($code))
            {
                return $code;
            }
        }
    }

}
