<?php

namespace IISModule;

use Nette\Application\UI\Form;
use InvalidArgumentException;
use Nette\DateTime;

/**
 * VehiclesPresenter
 *
 * @author Pavlína Bortlová <xbortl01@stud.fit.vutbr.cz>
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class VehiclesPresenter extends AuthenticatedPresenter
{

    /**
     * @service iis.vehicle
     * @var VehicleManager
     */
    protected $vehicleManager;

    /**
     * @service iis.vehicleTheft
     * @var VehicleTheftManager
     */
    protected $vehicleTheftManager;

    /**
     * @service iis.licensePlate
     * @var LicensePlateManager
     */
    protected $licensePlateManager;

    /**
     * @service iis.technicalCheck
     * @var TechnicalCheckManager
     */
    protected $technicalCheckManager;

    /**
     * @service iis.driver
     * @var DriverManager
     */
    protected $driverManager;

    /**
     * @var Vehicle
     */
    protected $vehicleEntity;

    /**
     *
     * @var VehicleTheft
     */
    protected $vehicleTheftEntity;

    protected function startup()
    {
        parent::startup();

        $this->requireRole(array(User::OFFICER, User::OFFICIAL, User::TECHNICIAN));
    }

    public function actionEdit($id)
    {
        $vehicle = $this->vehicleManager->findById($id);

        if (!$vehicle)
        {
            $this->flashMessage('Vozidlo nebylo nalezeno', 'error');
            $this->redirect('Vehicles:');
        }

        $this->vehicleEntity = $vehicle;
        $form                = $this->getComponent('vehicleEditForm');
        /* @var $form VehicleForm */

        $form->addIdRule($this->vehicleEntity->getId());
        $values                      = $vehicle->toArray();
        $values['dateOfManufacture'] = $values['dateOfManufacture']->format('Y-m-d');
        if ($form->isSubmitted())
        {
            $values = array_merge($values, $form->getValues(true));
        }

        $form->setValues($values);

        $grid = $this->getComponent('driverGrid');
        /* @var $grid DriverGrid */

        $grid->enableVehicleEditMode();
    }

    public function actionAddVehicleTheft($id)
    {
        $this->requireRole(User::OFFICER);

        $vehicle = $this->vehicleManager->findById($id);

        if (!$vehicle)
        {
            $this->flashMessage('Vozidlo nebylo nalezeno', 'error');
            $this->redirect('Vehicles:');
        }

        $this->vehicleEntity = $vehicle;
        $theftForm           = $this->getComponent('theftForm');
        /* @var $theftForm TheftForm */

        $theftForm->setValues(array('vehicle' => $id));
    }

    public function actionAddTechnicalCheck($id)
    {
        $this->requireRole(User::TECHNICIAN);

        $vehicle = $this->vehicleManager->findById($id);

        if (!$vehicle)
        {
            $this->flashMessage('Vozidlo nebylo nalezeno', 'error');
            $this->redirect('Vehicles:');
        }

        $this->vehicleEntity = $vehicle;
        $checkForm           = $this->getComponent('checkForm');
        /* @var $checkForm CheckForm */

        $checkForm->setValues(array('vehicle' => $id));
    }

    public function actionEditVehicleTheft($id)
    {
        $this->requireRole(User::OFFICER);

        $this->vehicleTheftEntity = $this->vehicleTheftManager->findById($id);

        if (!$this->vehicleTheftEntity)
        {
            $this->flashMessage('Krádež nebyla nalezena', 'error');
            $this->redirect('Vehicles:');
        }

        $theftForm = $this->getComponent('theftForm');
        /* @var $theftForm TheftForm */

        $values = array_merge($this->vehicleTheftEntity->toArray(), array('vehicle' => $this->vehicleTheftEntity->getVehicle()->getId()));

        $values['theftDate'] = $values['theftDate']->format('Y-m-d');

        if ($theftForm->isSubmitted())
        {
            $values = array_merge($values, $theftForm->getValues(true));
        }

        $theftForm->setValues($values);
    }

    public function actionAddLicensePlate($id)
    {
        $this->requireRole(User::OFFICIAL);

        $vehicle = $this->vehicleManager->findById($id);

        if (!$vehicle)
        {
            $this->flashMessage('Vozidlo nebylo nalezeno', 'error');
            $this->redirect('Vehicles:');
        }

        $this->vehicleEntity = $vehicle;
        $licensePlateForm    = $this->getComponent('licensePlateForm');
        /* @var $licensePlateForm CheckForm */

        $licensePlateForm->setValues(array('vehicle' => $id));
    }

    protected function createComponentVehicleAddForm($name)
    {
        $form = new VehicleForm($this, $name);

        $form->addIdRule();

        $form->onSuccess[] = array($this, 'onVehicleFormSuccess');

        return $form;
    }

    protected function createComponentVehicleEditForm($name)
    {
        $form = new VehicleForm($this, $name);

        $form->onSuccess[] = array($this, 'onVehicleFormSuccess');

        return $form;
    }

    protected function createComponentVehicleGrid($name)
    {
        $grid = new VehicleGrid($this, $name);

        $grid->disableFeaturesByUser($this->user);

        return $grid;
    }

    protected function createComponentTheftForm($name)
    {
        $form = new TheftForm($this, $name);

        $thefts = array();

        $form->onSuccess[] = array($this, 'onTheftFormSuccess');

        return $form;
    }

    protected function createComponentCheckForm($name)
    {
        $form = new CheckForm($this, $name);

        $checks = array();

        $form->onSuccess[] = array($this, 'onCheckFormSuccess');

        return $form;
    }

    protected function createComponentLicensePlateForm($name)
    {
        $form = new LicensePlateForm($this, $name);

        $parameters = $this->getContext()->getParameters();

        $regionsField = $form->getRegionsField();
        $regionsField->setItems($parameters['licensePlateRegions'], false);

        $form->onSuccess[] = array($this, 'onLicensePlateFormSuccess');

        return $form;
    }

    protected function createComponentDriverGrid($name)
    {
        $grid = new DriverGrid($this, $name);

        return $grid;
    }

    public function onVehicleFormSuccess(Form $form)
    {
        $values = $form->getValues();

        $values->offsetUnset('id');
        $values['dateOfManufacture'] = \Nette\DateTime::from($values['dateOfManufacture']);

        $vehicle = $this->vehicleEntity;

        if (!$vehicle)
        {
            $vehicle = $this->vehicleManager->createEntity();
        }

        $vehicle->fromArray($values);

        try
        {
            $this->vehicleManager->persist($vehicle, true);
            $this->flashMessage('Údaje byly uloženy', 'success');
            $this->redirect('Vehicles:edit', array('id' => $vehicle->getId()));
        }
        catch (InvalidArgumentException $e)
        {
            $this->flashMessage('Nepodařilo se vytvořit nové vozidlo', 'error');
        }
    }

    public function onTheftFormSuccess(Form $form)
    {
        $values = $form->getValues();

        if ($this->vehicleTheftEntity)
        {
            $theft = $this->vehicleTheftEntity;
        }
        else
        {
            $theft = $this->vehicleTheftManager->createEntity();
        }
        /* @var $theft VehicleTheft */


        $theft->setTheftDate(\Nette\DateTime::from($values['theftDate']));
        $theft->setLocation($values['location']);

        if (!$this->vehicleTheftEntity)
        {
            $theft->setVehicle($this->vehicleEntity);

            $this->vehicleEntity->addVehicleTheft($theft);
        }

        try
        {
            $this->vehicleTheftManager->persist($theft, true);
            $this->flashMessage('Krádež byla zaznamenána', 'success');
            $this->redirect('Vehicles:');
        }
        catch (InvalidArgumentException $e)
        {
            $this->flashMessage('Nepodařilo se zaznamenat krádež', 'error');
        }
    }

    public function onCheckFormSuccess(Form $form)
    {
        $values = $form->getValues();

        $check = $this->technicalCheckManager->createEntity();
        /* @var $check TechnicalCheck */
        $today = new DateTime();

        $check->setVehicleState($values['vehicleState']);
        $check->setCheckDate($today);
        $check->setValidUntil($today->modifyClone('+2 years'));
        $check->setVehicle($this->vehicleEntity);

        $this->vehicleEntity->addTechnicalCheck($check);

        try
        {
            $this->technicalCheckManager->persist($check, true);
            $this->flashMessage('Kontrola byla zaznamenána', 'success');
            $this->redirect('Vehicles:');
        }
        catch (InvalidArgumentException $e)
        {
            $this->flashMessage('Nepodařilo se zaznamenat kontrolu', 'error');
        }
    }

    public function onLicensePlateFormSuccess(Form $form)
    {
        $values = $form->getValues();

        if (($licensePlate = $this->vehicleEntity->getLicensePlate()) == null)
        {
            $licensePlate = $this->licensePlateManager->createEntity();

            $licensePlate->setVehicle($this->vehicleEntity);
            $this->vehicleEntity->setLicensePlate($licensePlate);
        }
        /* @var $licensePlate LicensePlate */
        $today = new DateTime();

        $licensePlate->setCode(\Nette\Utils\Strings::capitalize($values['code']));
        $licensePlate->setRegion($values['regions']);
        $licensePlate->setAssignDate($today);

        try
        {
            $this->licensePlateManager->persist($licensePlate, true);
            $this->flashMessage('SPZ byla zaznamenána', 'success');
            $this->redirect('Vehicles:');
        }
        catch (InvalidArgumentException $e)
        {
            $this->flashMessage('Nepodařilo se zaznamenat SPZ', 'error');
        }
    }

    public function handleSetDriver($driverId)
    {
        $driver = $this->driverManager->findById($driverId);

        if (!$driver)
        {
            $this->flashMessage('Řidič nebyl nalezen', 'error');
            return;
        }

        $this->vehicleEntity->setDriver($driver);

        try
        {
            $this->vehicleManager->persist($this->vehicleEntity, true);
            $this->flashMessage('Vlastník vozidla byl nastaven', 'success');
        }
        catch (InvalidArgumentException $e)
        {
            $this->flashMessage('Nepodařilo se nastavit vlastníka', 'error');
        }

        if($this->isAjax())
        {
            $this->invalidateControl('flashes');
        }
    }

}
