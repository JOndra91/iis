<?php

namespace IISModule;

use Nette\Application\UI\Presenter;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Presenter
{

    protected function startup()
    {
        parent::startup();

        $reflection = $this->getReflection();

        $properties = $reflection->getProperties();

        foreach ($properties as $property)
        {
            if (!is_null($service = $property->getAnnotation('service')))
            {
                $property->setAccessible(true);
                $property->setValue($this, $this->getContext()->getService($service));
            }
        }
    }

}
