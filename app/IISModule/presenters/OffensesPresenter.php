<?php

namespace IISModule;

/**
 * OffensesPresenter
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class OffensesPresenter extends AuthenticatedPresenter
{

    /**
     * @service iis.offenseDefinition
     * @var OffenseDefinitionManager
     */
    protected $offenseDefinitionManager;

    /**
     * @var OffenseDefinition
     */
    protected $offenseEntity;

    protected function startup()
    {
        parent::startup();

        $this->requireRole(User::OFFICIAL);
    }

    public function actionEdit($id)
    {
        $offense = $this->offenseDefinitionManager->findById($id);

        if (!$offense)
        {
            $this->flashMessage('Přestupek nebyl nalezen', 'error');
            $this->redirect('Offenses:');
        }

        $this->offenseEntity = $offense;
        $form                = $this->getComponent('offenseEditForm');
        /* @var $form OffenseDefinitionForm */

        $form->addIdRule($this->offenseEntity->getId());

        $values = $offense->toArray();
        if ($form->isSubmitted())
        {
            $values = array_merge($values, $form->getValues(true));
        }

        $form->setValues($values);
    }

    protected function createComponentOffenseAddForm($name)
    {
        $form = new OffenseDefinitionForm($this, $name);

        $form->addIdRule();

        $form->onSuccess[] = array($this, 'onOffenseFormSuccess');

        return $form;
    }

    protected function createComponentOffenseEditForm($name)
    {
        $form = new OffenseDefinitionForm($this, $name);

        $form->onSuccess[] = array($this, 'onOffenseFormSuccess');

        return $form;
    }

    public function onOffenseFormSuccess(OffenseDefinitionForm $form)
    {
        $values = $form->getValues();

        $values->offsetUnset('id');

        if ($this->offenseEntity)
        {
            $offense = $this->offenseEntity;
        }
        else
        {
            $offense = $this->offenseDefinitionManager->createEntity();
        }
        /* @var $offense OffenseDefinition */

        $offense->fromArray($values);

        try
        {
            $this->offenseDefinitionManager->persist($offense, true);
            $this->flashMessage('Definice přestupku byla uložena', 'success');
            $this->redirect('Offenses:edit', array('id' => $offense->getId()));
        }
        catch (InvalidArgumentException $e)
        {
            $this->flashMessage('Nepodařilo se uložit definici přestupku', 'error');
        }
    }

    protected function createComponentOffenseGrid($name)
    {
        return new OffenseDefinitionGrid($this, $name);
    }

    public function handleToggle($id)
    {
        $offense = $this->offenseDefinitionManager->findById($id);
        /* @var $offense OffenseDefinition */

        $offense->setEnabled(!$offense->getEnabled());

        try
        {
            $this->offenseDefinitionManager->persist($offense, true);
            $this->isAjax() && $this->getComponent('offenseGrid')->invalidateControl();
        }
        catch (InvalidArgumentException $e)
        {
            $this->flashMessage('Nepodařilo se povolit/zakázat definici přestupku', 'error');

            $this->isAjax() && $this->invalidateControl('flashes');
        }

        if (!$this->isAjax())
        {
            $this->redirect('Offenses:');
        }
    }

}
