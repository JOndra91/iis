<?php

namespace IISModule;

use InvalidArgumentException;
use Nette\Forms\Controls\TextInput;

/**
 * Users
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class UsersPresenter extends AuthenticatedPresenter
{

    /**
     * @service iis.user
     * @var UserManager
     */
    protected $userManager;

    /**
     * @service authenticator
     * @var Authenticator
     */
    protected $authenticator;

    /**
     * @var User
     */
    protected $userEntity;

    protected function startup()
    {
        parent::startup();

        $this->requireRole(USer::ADMIN);
    }

    public function actionEdit($id)
    {
        $user = $this->userManager->findById($id);

        if (!$user)
        {
            $this->flashMessage('Uživatel nebyl nalezen', 'error');
            $this->redirect('Users:');
        }

        $this->userEntity = $user;
        $form             = $this->getComponent('userEditForm');
        /* @var $form UserForm */

        $form->addIdRule($this->userEntity->getId());

        $values = $user->toArray();
        if ($form->isSubmitted())
        {
            $values = array_merge($values, $form->getValues(true));
        }

        $form->setValues($values);
    }

    public function onUserFormSuccess(UserForm $form)
    {
        $values = $form->getValues();

        $values->offsetUnset('id');

        $user = $this->userEntity;
        /* @var $user User */
        if ($user)
        {
            $values->offsetUnset('username');
        }
        else
        {
            $user = $this->userManager->createEntity();
        }

        if (empty($values['password']))
        {
            $values->offsetUnset('password');
        }
        else
        {
            $values['password'] = $this->authenticator->calculateHash($values['password']);
        }

        $user->fromArray($values);

        try
        {
            $this->userManager->persist($user, true);
            $this->flashMessage('Údaje byly uloženy', 'success');
            $this->redirect('Users:edit', array('id' => $user->getId()));
        }
        catch (InvalidArgumentException $e)
        {
            $this->flashMessage('Nepodařilo se vytvořit nového uživatele', 'error');
        }
    }

    protected function createComponentUserGrid($name)
    {
        return new UserGrid($this, $name);
    }

    protected function createComponentUserAddForm($name)
    {
        $form = new UserForm($this, $name);

        $form->addIdRule();
        $form->setPasswordRequired();
        $form->onSuccess[] = array($this, 'onUserFormSuccess');

        return $form;
    }

    protected function createComponentUserEditForm($name)
    {
        $form = new UserForm($this, $name);

        $username = $form['username'];
        /* @var $username TextInput */
        $username->setDisabled(true);

        $form->onSuccess[] = array($this, 'onUserFormSuccess');

        return $form;
    }

    public function handleDelete($id)
    {
        $user = $this->userManager->findById($id);
        /* @var $user User */

        if ($user->getId() == $this->user->getIdentity()->getId())
        {
            $this->flashMessage('Nelze smazat sám sebe', 'error');
        }
        else
        {
            try
            {
                $this->userManager->delete($user, null, true);
                $this->flashMessage("Uživatel '{$user->getUsername()}' byl smazán", 'success');
                $this->isAjax() && $this->getComponent('usersGrid')->invalidateControl();
            }
            catch (InvalidArgumentException $e)
            {
                $this->flashMessage("Uživatel '{$user->getUsername()}' nebyl smazán", 'error');
            }
        }

        if ($this->isAjax())
        {
            $this->invalidateControl('flashes');
        }
        else
        {
            $this->redirect('Users:');
        }
    }

}
