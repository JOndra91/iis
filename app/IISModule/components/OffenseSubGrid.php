<?php

namespace IISModule;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Query\Expr;
use Nette\Application\UI\Presenter;
use NiftyGrid\DoctrineDataSource;

/**
 * OffenseGrid
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class OffenseSubGrid extends BaseSubGrid
{

    /**
     * @service iis.offense
     * @var OffenseManager
     */
    protected $offenseManager;

    protected function configure(Presenter $presenter)
    {
        parent::configure($presenter);

        $qb     = $this->offenseManager->createQueryBuilder()
                ->select('o, d')
                ->from(Offense::ENTITY, 'o')
                ->join(OffenseDefinition::ENTITY, 'd', Expr\Join::WITH, 'o.offenseDefinition = d.id')
                ->where('o.driver = :driver')
                ->setParameter('driver', $this->parentId, Type::INTEGER);
        $source = new DoctrineDataSource($qb, 'o_id');

        $this->setDataSource($source);

        $this->addColumn('d_name', 'Přestupek')
                ->setCellRenderer(function($row)
                {
                    return 'font-weight: bold;';
                });

        $this->addColumn('d_description', 'Popis');

        $this->addColumn('o_date', 'Datum přestupku')
                ->setRenderer(self::dateRenderer('o_date'));

        $this->addColumn('o_place', 'Místo přestupku');

        $this->addColumn('o_penaltyPoints', 'Počet bodů');


        $this->addButton('edit', 'Upravit')
                ->setClass('edit')
                ->setAjax(false)
                ->setLink(function($row) use ($presenter)
                {
                    return $presenter->link('editOffense', $row['o_id']);
                });
    }

}
