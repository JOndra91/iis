<?php

namespace IISModule;

/**
 * BaseSubGrid
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class BaseSubGrid extends BaseGrid
{

    public function __construct($name, $parentId)
    {
        parent::__construct(null, $name);
        $this->setParentId($parentId);
    }

}
