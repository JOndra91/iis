<?php

namespace IISModule;

use Nette\Application\UI\Presenter;
use NiftyGrid\DoctrineDataSource;

/**
 * OffenseDefinitionGrid
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class OffenseDefinitionGrid extends BaseGrid
{

    /**
     * @service iis.offenseDefinition
     * @var OffenseDefinitionManager
     */
    protected $offenseDefinitionManager;

    protected function configure(Presenter $presenter)
    {
        parent::configure($presenter);

        $source = new DoctrineDataSource($this->offenseDefinitionManager->createQueryBuilder('o'), 'o_id');

        $this->setDataSource($source);

        $this->addColumn('o_name', 'Přestupek')
                ->setCellRenderer(function($row){
                    return 'font-weight: bold;';
                })
                ->setTextFilter();
        $this->addColumn('o_description', 'Popis přestupku');
        $this->addColumn('o_penaltyPoints', 'Počet bodů v bodovém systému')
                ->setNumericFilter();
        $this->addColumn('o_penalty', 'Výše pokuty [Kč]');

        $this->addColumn('o_enabled', 'Udělování povoleno')
                ->setBooleanFilter()
                ->setRenderer(function($row)
                {
                    return $row['o_enabled'] ? 'Ano' : 'Ne';
                });

        $this->addButton('edit', 'Upravit')
                ->setClass('edit')
                ->setAjax(false)
                ->setLink(function($row) use ($presenter)
                {
                    return $presenter->link('edit', $row['o_id']);
                });

        $this->addButton('toggle')
                ->setLabel(function($row)
                {
                    return $row['o_enabled'] ? 'Zakázat' : 'Povolit';
                })
                ->setClass(function($row)
                {
                    return $row['o_enabled'] ? 'trash' : 'restore';
                })
                ->setLink(function ($row) use ($presenter)
                {
                    return $presenter->link('toggle!', $row['o_id']);
                });
    }

}
