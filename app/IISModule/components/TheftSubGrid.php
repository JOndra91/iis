<?php

namespace IISModule;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Query\Expr;
use Nette\Application\UI\Presenter;
use NiftyGrid\DoctrineDataSource;

/**
 * TheftSubGrid
 *
 * @author Pavlína Bortlová <xbortl01@stud.fit.vutbr.cz>
 */
class TheftSubGrid extends BaseSubGrid
{

    /**
     * @service iis.vehicleTheft
     * @var vehicleTheftManager
     */
    protected $vehicleTheftManager;

    protected function configure(Presenter $presenter)
    {
        parent::configure($presenter);

        $qb = $this->vehicleTheftManager->createQueryBuilder()
                ->select('t')
                ->from(VehicleTheft::ENTITY, 't')
                ->where('t.vehicle = :vehicle')
                ->orderBy('t.theftDate', 'desc')
                ->orderBy('t.id', 'desc')
                ->setParameter('vehicle', $this->parentId, Type::INTEGER);

        $source = new DoctrineDataSource($qb, 't_id');

        $this->setDataSource($source);

        $this->addColumn('t_theftDate', 'Datum krádeže')
                ->setRenderer(self::dateRenderer('t_theftDate'));

        $this->addColumn('t_location', 'Místo krádeže');

        $this->addButton('edit', 'Upravit')
                ->setClass('edit')
                ->setAjax(false)
                ->setLink(function($row) use ($presenter)
                {
                    return $presenter->link('editVehicleTheft', $row['t_id']);
                });
    }

}
