<?php

namespace IISModule;

use Nette\Application\UI\Presenter;
use Nette\ComponentModel\IContainer;
use NiftyGrid\DoctrineDataSource;

/**
 * UserGrid
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class UserGrid extends BaseGrid
{

    /**
     * @service iis.user
     * @var UserManager
     */
    protected $userManager;

    protected function configure(Presenter $presenter)
    {
        parent::configure($presenter);

        $roles = array(
            'admin'    => 'Správce',
            'official' => 'Úředník',
            'officer'  => 'Policista',
            'technician' => 'Technik'
        );

        $source = new DoctrineDataSource($this->userManager->createQueryBuilder('u'), 'u_id');

        $this->setDataSource($source);

        $this->addColumn('u_name', 'Jméno')
                ->setTextFilter();
        $this->addColumn('u_surname', 'Příjmení')
                ->setTextFilter();
        $this->addColumn('u_username', 'Přihlašovací jméno')
                ->setTextFilter();
        $this->addColumn('u_role', 'Role')
                ->setRenderer(function($row) use ($roles)
                {
                    return $roles[$row['u_role']];
                });
        $this->addButton('edit', 'Upravit')
                ->setClass('edit')
                ->setAjax(false)
                ->setLink(function($row) use ($presenter)
                {
                    return $presenter->link('edit', $row['u_id']);
                });
        $this->addButton('delete', 'Smazat')
                ->setClass('delete')
                ->setLink(function($row) use ($presenter)
                {
                    return $presenter->link('delete!', $row['u_id']);
                })
                ->setConfirmationDialog(function($row)
                {
                    return "Určitě chcete smazat uživatele '{$row['u_username']}'?";
                });
    }

}
