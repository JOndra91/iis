<?php

namespace IISModule;

use Nette\Application\UI\Presenter;
use Nette\DateTime;
use Nette\Security\User as SecurityUser;
use NiftyGrid\Button;
use NiftyGrid\DoctrineDataSource;
use Doctrine\ORM\Query\Expr;

/**
 * DriverGrid
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class DriverGrid extends BaseGrid
{

    /**
     * @service iis.driver
     * @var DriverManager
     */
    protected $driverManager;

    /*
     * @var Button
     */
    protected $editButton;

    /**
     * @var Button
     */
    protected $offenseButton;

    /**
     * @var Button
     */
    protected $licenseButton;
    protected $offenseSubGrid;
    protected $licenseSubGrid;
    protected $vehicleSubgrid;
    protected $subGridMode = false;

    protected function configure(Presenter $presenter)
    {
        parent::configure($presenter);

        $qb = $this->driverManager->createQueryBuilder()
                ->select('d, SUM(o.penaltyPoints) as penaltyPoints')
                ->from(Driver::ENTITY, 'd')
                ->leftJoin(Offense::ENTITY, 'o', Expr\Join::WITH, 'd.id = o.driver')
                ->groupBy('d.id');

        if ($this->parentId)
        {
            $qb->leftJoin(Vehicle::ENTITY, 'v', Expr\Join::WITH, 'd.id = v.driver')
                    ->where('v.id = :vehicle')
                    ->setParameter('vehicle', $this->parentId);
        }

        $source = new DoctrineDataSource($qb, 'd_id');

        $this->setDataSource($source);

        $today = new DateTime();

        if (!$this->subGridMode)
        {
            $this->vehicleSubgrid = $this->addSubGrid('vehicle', 'Vozidla');

            $vehicleGrid = new VehicleGrid(null, 'vehicleSubGrid');
            $vehicleGrid->setParentId($this->activeSubGridId);
            $vehicleGrid->enableSubGridMode();

            $this->vehicleSubgrid->setGrid($vehicleGrid);

            $theftSubGrid = new TheftSubGrid('theftSubGrid', $this->activeSubGridId);
            $this->addSubGrid('theft', 'Krádež')
                    ->setGrid($theftSubGrid);
        }

        $offenseSubGrid = new OffenseSubGrid('offenseSubGrid', $this->activeSubGridId);

        $this->offenseSubGrid = $this->addSubGrid('offense', 'Přestupky')
                ->setGrid($offenseSubGrid);

        $licenseSubGrid = new LicenseSubGrid('licenseSubGrid', $this->activeSubGridId);

        $this->licenseSubGrid = $this->addSubGrid('license', 'Řidičský průkaz')
                ->setGrid($licenseSubGrid);

        $this->addColumn('d_name', 'Jméno')
                ->setTextFilter();
        $this->addColumn('d_surname', 'Příjmení')
                ->setTextFilter();
        $this->addColumn('d_birthCode', 'Rodné číslo')
                ->setTextFilter();
        $this->addColumn('d_birthDate', 'Datum narození')
                ->setRenderer(function($row)
                {
                    return $row['d_birthDate']->format('d.m.Y');
                });
        $this->addColumn('d_drivingRestricted', 'Zákaz řízení')
                ->setRenderer(function($row) use ($today)
                {
                    return ($row['d_drivingRestricted'] < $today) ? 'Ne' : "do {$row['d_drivingRestricted']->format('d.m.Y')}";
                });

        $this->addColumn('penaltyPoints', 'Počet bodů');

        if (!$this->subGridMode)
        {

            $this->editButton = $this->addButton('edit', 'Upravit');
            $this->editButton
                    ->setClass('edit')
                    ->setAjax(false)
                    ->setLink(function($row) use ($presenter)
                    {
                        return $presenter->link('edit', $row['d_id']);
                    });

            $this->offenseButton = $this->addButton('offense', 'Přidat přestupek');
            $this->offenseButton
                    ->setClass('add')
                    ->setAjax(false)
                    ->setLink(function($row) use ($presenter)
                    {
                        return $presenter->link('addOffense', $row['d_id']);
                    });

            $this->licenseButton = $this->addButton('license', 'Vyřídit průkaz');
            $this->licenseButton
                    ->setClass('add')
                    ->setAjax(false)
                    ->setLink(function($row) use ($presenter)
                    {
                        return $presenter->link('addLicense', $row['d_id']);
                    });
        }
    }

    /**
     * @param SecurityUser $user
     */
    public function disableFeaturesByUser(SecurityUser $user)
    {
        $buttonContainer = $this['buttons'];

        if (!$user->isInRole(User::OFFICIAL))
        {
            $buttonContainer->removeComponent($this->editButton);
            $buttonContainer->removeComponent($this->licenseButton);
        }

        if (!$user->isInRole(User::OFFICER))
        {
            $buttonContainer->removeComponent($this->offenseButton);
        }
    }

    public function enableVehicleEditMode()
    {
        $presenter = $this->getPresenter();

        $container = $this['buttons'];
        $container->removeComponent($this->editButton);
        $container->removeComponent($this->licenseButton);
        $container->removeComponent($this->offenseButton);

        $container = $this['subGrids'];
        $container->removeComponent($this->offenseSubGrid);
        $container->removeComponent($this->licenseSubGrid);

        $this->addButton('driver', 'Nastavit vlastníka')
                ->setClass('add')
                ->setLink(function($row) use ($presenter)
                {
                    return $presenter->link('setDriver!', $row['d_id']);
                });
    }

    public function enableSubGridMode()
    {
        $this->subGridMode = true;
    }

}
