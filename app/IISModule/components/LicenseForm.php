<?php

namespace IISModule;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls\HiddenField;
use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\TextInput;

/**
 * LicenseForm
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class LicenseForm extends BaseForm
{

    /**
     * @var TextInput
     */
    protected $codeField;

    /**
     * @var MultiSelectBox
     */
    protected $categoriesField;

    public function __construct(IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->addHidden('driver');

        $this->addText('code', 'Číslo řidičského průkazu')
                ->setRequired('%label musí být vyplněno')
                ->setAttribute('readonly', 'readonly');

        $this->categoriesField = $this->addMultiSelect('categories', 'Skupiny řidičkého průkazu')
                ->setRequired('%label musí být vybrány');

        $this->addSubmit('save', 'Uložit');
    }

    /**
     * @return MultiSelectBox
     */
    public function getCategoriesField()
    {
        return $this->categoriesField;
    }

}
