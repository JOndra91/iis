<?php

namespace IISModule;

use Doctrine\DBAL\Types\Type;
use Nette\Application\UI\Presenter;
use NiftyGrid\DoctrineDataSource;

/**
 * LicenseSubGrid
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class LicenseSubGrid extends BaseSubGrid
{

    /**
     * @service iis.driverLicense
     * @var DriverLicenseManager
     */
    protected $driverLicenseManager;

    protected function configure(Presenter $presenter)
    {
        parent::configure($presenter);

        $qb = $this->driverLicenseManager->createQueryBuilder()
                ->select('l')
                ->from(DriverLicense::ENTITY, 'l')
                ->where('l.driver = :driver')
                ->orderBy('l.issueDate', 'desc')
                ->orderBy('l.id', 'desc')
                ->setParameter('driver', $this->parentId, Type::INTEGER);

        $source = new DoctrineDataSource($qb, 'l_id');

        $this->setDataSource($source);
        $this->paginate = false;

        $this->addColumn('l_code', 'Číslo průkazu');

        $this->addColumn('l_issueDate', 'Datum vyřízení')
                ->setRenderer(self::dateRenderer('l_issueDate'));

        $this->addColumn('l_validUntil', 'Platnost')
                ->setRenderer(self::dateRenderer('l_validUntil'));

        $this->addColumn('l_categories', 'Skupiny');

    }

}
