<?php

namespace IISModule;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls\HiddenField;
use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\TextInput;

/**
 * LicensePlateForm
 *
 * @author Pavlína Bortlová <xbortl01@stud.fit.vutbr.cz>
 */
class LicensePlateForm extends BaseForm
{

    /**
     * @var TextInput
     */
    protected $codeField;

    /**
     * @var SelectBox
     */
    protected $regionsField;

    public function __construct(IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->addHidden('vehicle');

        $this->regionsField=$this->addSelect('regions', 'Kraj')
                ->setRequired('%label musí být vybrány');

        $this->addText('code', 'Kód SPZ')
                ->setRequired('%label musí být vyplněno')
                ->addRule(Form::LENGTH,'SPZ musí mít 7 znaků',7)
                ->addRule(Form::PATTERN,'SPZ musí být v daném formátu','\\d\\w[\\w\\d]\\d{4}');
        
        $this->addSubmit('save', 'Uložit');
    }

    /**
     * @return SelectBox
     */
    public function getRegionsField()
    {
        return $this->regionsField;
    }
}
