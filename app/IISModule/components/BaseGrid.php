<?php

namespace IISModule;

use NiftyGrid\Grid;

/**
 * BaseGrid
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class BaseGrid extends Grid
{

    /**
     * Identifikátor v nadřazeném gridu
     * @param mixed
     */
    protected $parentId;

    protected function configure(\Nette\Application\UI\Presenter $presenter)
    {
        parent::configure($presenter);

        $reflection = $this->getReflection();

        $properties = $reflection->getProperties();

        foreach ($properties as $property)
        {
            if (!is_null($service = $property->getAnnotation('service')))
            {
                $property->setAccessible(true);
                $property->setValue($this, $presenter->getContext()->getService($service));
            }
        }
    }

    protected static function dateRenderer($column, $format = 'd.m.Y')
    {
        return function($row) use ($column, $format)
        {
            if ($row[$column])
            {
                return $row[$column]->format($format);
            }
            else
            {
                return "Neznámé";
            }
        };
    }

    public function setParentId($id)
    {
        $this->parentId = $id;
    }

}
