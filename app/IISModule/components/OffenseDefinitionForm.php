<?php

namespace IISModule;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;

/**
 * OffenseDefinitionForm
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class OffenseDefinitionForm extends EntityForm
{

    public function __construct(IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->addText('name', 'Přestupek', 128, 255)
                ->setRequired('%label musí být vyplněn');

        $this->addTextArea('description', 'Popis přestupku');

        $this->addText('penaltyPoints', 'Počet bodů')
                ->setType('number')
                ->setRequired('%label musí být vyplněn')
                ->addRule(Form::NUMERIC, 'Hodnota %label musí být celé číslo')
                ->addRule(Form::RANGE, 'Hodnota %label musí být v rozmezí od %d do %d', array(0, 12));

//        $penalty = new FloatInput();
        $penalty = $this->addText('penalty', 'Výše pokuty')
                ->setRequired('%label musí být vyplněna');

//        $this->addComponent($penalty, 'penalty');


        $this->addSubmit('save', 'Uložit');
    }

    public function render()
    {
        parent::render();
    }

}
