<?php

namespace IISModule;

use Doctrine\DBAL\Types\Type;
use Nette\Application\UI\Presenter;
use NiftyGrid\DoctrineDataSource;

/**
 * LicensePlateSubGrid
 *
 * @author Pavlína Bortlová <xbortl01@stud.fit.vutbr.cz>
 */
class LicensePlateSubGrid extends BaseSubGrid
{

    /**
     * @service iis.licensePlate
     * @var LicensePlateManager
     */
    protected $licensePlateManager;

    protected function configure(Presenter $presenter)
    {
        parent::configure($presenter);

        $qb = $this->licensePlateManager->createQueryBuilder()
                ->select('p')
                ->from(LicensePlate::ENTITY, 'p')
                ->where('p.vehicle = :vehicle')
                ->orderBy('p.assignDate', 'desc')
                ->orderBy('p.id', 'desc')
                ->setMaxResults(1)
                ->setParameter('vehicle', $this->parentId, Type::INTEGER);

        $source = new DoctrineDataSource($qb, 'p_id');

        $this->setDataSource($source);
        $this->paginate = false;

        $this->addColumn('p_region', 'Kraj');

        $this->addColumn('p_code', 'SPZ');


        $this->addColumn('p_assignDate', 'Datum vystavení')
                ->setRenderer(self::dateRenderer('p_assignDate'));
    }

}
