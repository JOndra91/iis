<?php

namespace IISModule;

use Nette\DateTime;
use Nette\Forms\IControl;

/**
 * CustomFormValidators
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
abstract class CustomFormValidators
{

    /**
     * Zda zadané datum není v minulosti
     * @param \Nette\DateTime $date
     * @return boolean
     */
    static function pastDate($date = null)
    {
        if (!$date)
        {
            $date = new DateTime();
        }

        return function(IControl $control) use ($date)
        {
            $value = DateTime::from($control->getValue());

            return $value >= $date;
        };
    }

}
