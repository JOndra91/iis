<?php

namespace IISModule;

use Nette\Forms\Controls\TextInput;

/**
 * FloatInput
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class FloatInput extends TextInput
{

    public function getControl()
    {
        $control = parent::getControl();

        if (!empty($control->value))
        {
            $control->value = number_format(floatval($control->value), 2, '.', '');
        }

        return $control;
    }

}
