<?php

namespace IISModule;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls\HiddenField;
use Nette\Forms\Controls\SelectBox;

/**
 * TheftForm
 *
 * @author Pavlína Bortlová <xbortl01@stud.fit.vutbr.cz>
 */
class TheftForm extends BaseForm
{

    /**
     * @var HiddenField
     */
    protected $vehicleField;

    public function __construct(IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->vehicleField = $this->addHidden('vehicle');

        $this->addText('theftDate', 'Datum krádeže')
                ->setType('date')
                ->setRequired('%label musí být vyplněno')
                ->setValue(date('Y-m-d'))
                ->addRule(Form::PATTERN, '%label musí být ve formátu RRRR-MM-DD', '\\d{4}-\\d{2}-\\d{2}');

        $this->addTextArea('location', 'Místo krádeže')
                ->setRequired('%label musí být vyplněno');        

        $this->addSubmit('save', 'Uložit');
    }

    public function addVehicleRule($id)
    {
        $this->vehicleField->addRule(Form::EQUAL, 'Formulář není validní', $id);
    }


}
