<?php

namespace IISModule;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls\HiddenField;

/**
 * EntityForm
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class EntityForm extends BaseForm
{

    /**
     * @var HiddenField
     */
    protected $idField;

    public function __construct(IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->idField = $this->addHidden('id', 0);
    }

    public function addIdRule($id = null)
    {
        $this->idField->addRule(Form::EQUAL, 'Formulář není validní', intval($id));
    }

}
