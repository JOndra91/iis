<?php

namespace IISModule;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls\HiddenField;
use Nette\Forms\Controls\SelectBox;

/**
 * OffenseForm
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class OffenseForm extends BaseForm
{

    /**
     * @var SelectBox
     */
    protected $offenseSelect;

    /**
     * @var HiddenField
     */
    protected $driverField;

    public function __construct(IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->driverField = $this->addHidden('driver');

        $this->offenseSelect = $this->addSelect('offense', 'Přestupek');
        $this->offenseSelect
                ->setPrompt('--- Vyberte přestupek ---')
                ->setRequired('%label musí být vybrán');

        $this->addTextArea('place', 'Místo přestupku')
                ->setRequired('%label musí být vyplněno');

        $this->addText('date', 'Datum přestupku')
                ->setType('date')
                ->setRequired('%label musí být vyplněno')
                ->setValue(date('Y-m-d'))
                ->addRule(Form::PATTERN, '%label musí být ve formátu RRRR-MM-DD', '\\d{4}-\\d{2}-\\d{2}');

        $this->addSubmit('save', 'Uložit');
    }

    public function addDriverRule($id)
    {
        $this->driverField->addRule(Form::EQUAL, 'Formulář není validní', $id);
    }

    public function setOffenseItems(array $items)
    {
        $this->offenseSelect->setItems($items);
    }

}
