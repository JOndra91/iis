<?php

namespace IISModule;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls\TextInput;

/**
 * UserForm
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class UserForm extends EntityForm
{

    /**
     * @var TextInput
     */
    protected $password;

    public function __construct(IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $roles = array(
            'admin'    => 'Správce',
            'official' => 'Úředník',
            'officer'  => 'Policista',
            'technician' => 'Technik'
        );

        $this->addText('name', 'Jméno')
                ->setRequired('%label musí být vyplněno');
        $this->addText('surname', 'Příjmení')
                ->setRequired('%label musí být vyplněno');
        $this->addText('username', 'Přihlašovací jméno')
                ->setRequired('%label musí být vyplněno')
                ->addRule(Form::MIN_LENGTH, '%label musí mít alespoň %d znaků', 5);
        $this->password = $this->addPassword('password', 'Heslo');
        $this->password
                ->addCondition(Form::FILLED)
                ->addRule(Form::MIN_LENGTH, '%label musí mít alespoň %d znaků', 6);
        $this->addSelect('role', 'Role', $roles);

        $this->addSubmit('save', 'Uložit');
    }

    public function setPasswordRequired()
    {
        $this->password->setRequired('%label musí být vyplněno');
    }

}
