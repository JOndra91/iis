<?php

namespace IISModule;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;

/**
 * DriverForm
 *
 * @author Pavlína Bortlová <xbortl01@stud.fit.vutbr.cz>
 */
class VehicleForm extends EntityForm
{

    public function __construct(IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->addText('serialNumber', 'Sériové číslo')
                ->setRequired('%label musí být vyplněno')
                ->addRule(Form::LENGTH, '%label musí obsahovat 17 znaků', '17')
                ->addRule(Form::PATTERN, '%label může obsahovat jen alfanumerické znaky ', '[\\w\\d]+');
        $this->addText('manufacturer', 'Výrobce')
                ->setRequired('%label musí být vyplněno');
        $this->addText('model', 'Model')
                ->setRequired('%label musí být vyplněno');
        $this->addText('dateOfManufacture', 'Datum výroby')
                ->setType('date')
                ->setRequired('%label musí být vyplněno')
                ->addRule(Form::PATTERN, '%label musí být ve formátu RRRR-MM-DD', '\\d{4}-\\d{2}-\\d{2}');
        $this->addText('fuelType', 'Palivo')
                ->setRequired('%label musí být vyplněno');
        $this->addText('weight', 'Hmotnost')
                ->setType('number')
                ->addRule(Form::NUMERIC, 'Hodnota %label musí být celé číslo')
                ->setRequired('%label musí být vyplněno');
        $this->addText('passengerCapacity', 'Počet míst k sezení')
                ->setType('number')
                ->addRule(Form::NUMERIC, 'Hodnota %label musí být celé číslo')
                ->setRequired('%label musí být vyplněno');
        $this->addText('maxSpeed', 'Maximální rychlost')
                ->setType('number')
                ->addRule(Form::NUMERIC, 'Hodnota %label musí být celé číslo')
                ->setRequired('%label musí být vyplněno');

        $this->addSubmit('save', 'Uložit');
    }

}
