<?php

namespace IISModule;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;

/**
 * DriverForm
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class DriverForm extends EntityForm
{

    public function __construct(IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->addText('name', 'Jméno')
                ->setRequired('%label musí být vyplněno');
        $this->addText('surname', 'Příjmení')
                ->setRequired('%label musí být vyplněno');
        $this->addText('birthCode', 'Rodné číslo')
                ->setRequired('%label musí být vyplněno')
                ->addRule(Form::PATTERN, '%label musí být ve formátu RRMMDD/XXXX', '\\d{6}/\\d{4}');
        $this->addText('birthDate', 'Datum narození')
                ->setType('date')
                ->setRequired('%label musí být vyplněno')
                ->addRule(Form::PATTERN, '%label musí být ve formátu RRRR-MM-DD', '\\d{4}-\\d{2}-\\d{2}');
        $this->addText('drivingRestricted', 'Zákaz řízení do')
                ->setType('date')
                ->addCondition(Form::FILLED)
                ->addRule(Form::PATTERN, '%label musí být prázdný nebo ve formátu RRRR-MM-DD', '\\d{4}-\\d{2}-\\d{2}');

        $this->addSubmit('save', 'Uložit');
    }

}
