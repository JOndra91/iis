<?php

namespace IISModule;

use Nette\Application\UI\Presenter;
use Nette\DateTime;
use Nette\Security\User as SecurityUser;
use NiftyGrid\Button;
use NiftyGrid\DoctrineDataSource;
use Doctrine\ORM\Query\Expr;

/**
 * VehicleGrid
 *
 * @author Pavlína Bortlová <xbortl01@stud.fit.vutbr.cz>
 */
class VehicleGrid extends BaseGrid
{

    /**
     * @service iis.vehicle
     * @var VehicleManager
     */
    protected $vehicleManager;

    /*
     * @var Button
     */
    protected $editButton;

    /**
     * @var Button
     */
    protected $theftButton;

    /**
     * @var Button
     */
    protected $techCheckButton;

    /**
     * @var Button
     */
    protected $licensePlateButton;
    protected $driverSubgrid;
    protected $subGridMode = false;

    protected function configure(Presenter $presenter)
    {
        parent::configure($presenter);

        $parameters = $presenter->getContext()->getParameters();

        $qb = $this->vehicleManager->createQueryBuilder()
                ->select('v, l')
                ->from(Vehicle::ENTITY, 'v')
                ->leftJoin(LicensePlate::ENTITY, 'l', Expr\Join::WITH, 'v.id = l.vehicle');

        if ($this->parentId)
        {
            $qb->where('v.driver = :driver')
                    ->setParameter('driver', $this->parentId);
        }

        $source = new DoctrineDataSource($qb, 'v_id');

        $this->setDataSource($source);

        if (!$this->subGridMode)
        {

            $this->driverSubgrid = $this->addSubGrid('driver', 'Vlastník');

            $driverGrid = new DriverGrid(null, 'driverSubGrid');
            $driverGrid->setParentId($this->activeSubGridId);
            $driverGrid->enableSubGridMode();

            $this->driverSubgrid->setGrid($driverGrid);
        }

        $theftSubGrid = new TheftSubGrid('theftSubGrid', $this->activeSubGridId);
        $this->addSubGrid('theft', 'Krádež')
                ->setGrid($theftSubGrid);

//        $licensePlateSubGrid = new LicensePlateSubGrid('licensePlateSubGrid', $this->activeSubGridId);
//        $this->addSubGrid('licensePlate', 'SPZ')
//                ->setGrid($licensePlateSubGrid);

        $checkSubGrid = new CheckSubGrid('checkSubGrid', $this->activeSubGridId);
        $this->addSubGrid('check', 'STK')
                ->setGrid($checkSubGrid);


        $this->addColumn('v_serialNumber', 'Sériové číslo')
                ->setTextFilter();
        $this->addColumn('v_manufacturer', 'Výrobce')
                ->setTextFilter();
        $this->addColumn('v_model', 'Model')
                ->setTextFilter();
        $this->addColumn('v_dateOfManufacture', 'Datum výroby')
                ->setRenderer(function($row)
                {
                    return $row['v_dateOfManufacture']->format('d.m.Y');
                });
        $this->addColumn('v_fuelType', 'Palivo')
                ->setTextFilter();
        $this->addColumn('v_weight', 'Hmotnost [kg]')
                ->setNumericFilter()
                ->setRenderer(function ($row)
                {
                    return number_format($row['v_weight'], 0, '.', '');
                });
        $this->addColumn('v_passengerCapacity', 'Počet míst k sezení')
                ->setNumericFilter();
        $this->addColumn('v_maxSpeed', 'Maximalní rychlost [km/h]')
                ->setNumericFilter();

        $this->addColumn('l_code', "SPZ")
                ->setTextFilter();

        $this->addColumn('l_region', "Kraj")
                ->setSelectFilter($parameters['licensePlateRegions']);

        $this->addColumn("l_assignDate")
                ->setRenderer($this->dateRenderer('l_assignDate'))
                ->setDateFilter();

        if (!$this->subGridMode)
        {

            $this->editButton = $this->addButton('edit', 'Upravit');
            $this->editButton
                    ->setClass('edit')
                    ->setAjax(false)
                    ->setLink(function($row) use ($presenter)
                    {
                        return $presenter->link('edit', $row['v_id']);
                    });

            $this->techCheckButton = $this->addButton('technicalCheck', 'Provést technickou kontrolu');
            $this->techCheckButton
                    ->setClass('add')
                    ->setAjax(false)
                    ->setLink(function($row) use ($presenter)
                    {
                        return $presenter->link('addTechnicalCheck', $row['v_id']);
                    });

            $this->licensePlateButton = $this->addButton('licensePlate', 'Vyřídit SPZ');
            $this->licensePlateButton
                    ->setClass('add')
                    ->setAjax(false)
                    ->setLink(function($row) use ($presenter)
                    {
                        return $presenter->link('addLicensePlate', $row['v_id']);
                    });

            $this->theftButton = $this->addButton('theft', 'Přidat krádež');
            $this->theftButton
                    ->setClass('add')
                    ->setAjax(false)
                    ->setLink(function($row) use ($presenter)
                    {
                        return $presenter->link('addVehicleTheft', $row['v_id']);
                    });
        }
    }

    /**
     * @param SecurityUser $user
     */
    public function disableFeaturesByUser(SecurityUser $user)
    {
        $container = $this['buttons'];
        if (!$user->isInRole(User::OFFICIAL))
        {
            $container->removeComponent($this->editButton);
            $container->removeComponent($this->licensePlateButton);
        }
        if (!$user->isInRole(User::OFFICER))
        {
            $container->removeComponent($this->theftButton);
        }
        if (!$user->isInRole(User::TECHNICIAN))
        {
            $container->removeComponent($this->techCheckButton);
        }
    }

    public function enableSubGridMode()
    {
        $this->subGridMode = true;
    }

}
