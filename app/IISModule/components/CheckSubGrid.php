<?php

namespace IISModule;

use Doctrine\DBAL\Types\Type;
use Nette\Application\UI\Presenter;
use NiftyGrid\DoctrineDataSource;

/**
 * CheckSubGrid
 *
 * @author Pavlína Bortlová <xbortl01@stud.fit.vutbr.cz>
 */
class CheckSubGrid extends BaseSubGrid
{

    /**
     * @service iis.technicalCheck
     * @var TechnicalCheckManager
     */
    protected $technicalCheckManager;

    protected function configure(Presenter $presenter)
    {
        parent::configure($presenter);

        $qb = $this->technicalCheckManager->createQueryBuilder()
                ->select('c')
                ->from(TechnicalCheck::ENTITY, 'c')
                ->where('c.vehicle = :vehicle')
                ->orderBy('c.checkDate', 'desc')
                ->orderBy('c.id', 'desc')
                ->setMaxResults(1)
                ->setParameter('vehicle', $this->parentId, Type::INTEGER);

        $source = new DoctrineDataSource($qb, 'c_id');

        $this->setDataSource($source);
        $this->paginate = false;

        $this->addColumn('c_vehicleState', 'Stav kontroly');

        $this->addColumn('c_checkDate', 'Datum vystavení')
                ->setRenderer(self::dateRenderer('c_checkDate'));

        $this->addColumn('c_validUntil', 'Platnost')
                ->setRenderer(self::dateRenderer('c_validUntil'));
    }

}
