<?php

namespace IISModule;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;

/**
 * User
 * @Entity
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class User extends BaseEntity
{

    const ADMIN      = 'admin';
    const OFFICIAL   = 'official';
    const OFFICER    = 'officer';
    const TECHNICIAN = 'technician';
    //
    const ENTITY     = 'IISModule\\User';

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string", length=50, unique=true, nullable=false)
     * @var string
     */
    protected $username;

    /**
     * @Column(type="string", length=60, unique=false, nullable=false)
     * @var string
     */
    protected $password;

    /**
     * @Column(type="string", length=32, unique=false, nullable=false)
     * @var string
     */
    protected $role;

    /**
     * @Column(type="string", length=128, unique=false, nullable=false)
     * @var string
     */
    protected $name;

    /**
     * @Column(type="string", length=128, unique=false, nullable=false)
     * @var string
     */
    protected $surname;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

}
