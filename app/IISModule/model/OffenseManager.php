<?php

namespace IISModule;

/**
 * Offense manager
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class OffenseManager extends BaseEntityManager
{

    /**
     * @param \IISModule\EntityDefinition $definition
     * @return Offense
     */
    public function createEntityFromDefinition(OffenseDefinition $definition)
    {
        $entity = parent::createEntity();
        /* @var $entity Offense */

        $entity->setOffenseDefinition($definition);
        $entity->setPenaltyPoints($definition->getPenaltyPoints());

        return $entity;
    }

}
