<?php

namespace IISModule;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Nette\DateTime;

/**
 * TechnicalCheck
 * @Entity
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class TechnicalCheck extends BaseEntity
{

    const ENTITY = 'IISModule\\TechnicalCheck';

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="IISModule\Vehicle", inversedBy="technicalChecks")
     * @var Vehicle
     */
    protected $vehicle;

    /**
     * @Column(type="text", nullable=false)
     * @var string
     */
    protected $vehicleState;

    /**
     * @Column(type="date", nullable=false)
     * @var DateTime
     */
    protected $checkDate;

    /**
     * @Column(type="date", nullable=false)
     * @var DateTime
     */
    protected $validUntil;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Vehicle
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    public function setVehicle(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
        return $this;
    }

    public function getVehicleState()
    {
        return $this->vehicleState;
    }

    public function setVehicleState($vehicleState)
    {
        $this->vehicleState = $vehicleState;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCheckDate()
    {
        return $this->checkDate;
    }

    public function setCheckDate(DateTime $checkDate)
    {
        $this->checkDate = $checkDate;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    public function setValidUntil(DateTime $validUntil)
    {
        $this->validUntil = $validUntil;
        return $this;
    }

}
