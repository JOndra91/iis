<?php

namespace IISModule;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Nette\DateTime;

/**
 * Offense
 * @Entity
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class Offense extends BaseEntity
{

    const ENTITY = 'IISModule\\Offense';

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="IISModule\Driver", inversedBy="offenses")
     * @var Driver
     */
    protected $driver;

    /**
     * @ManyToOne(targetEntity="IISModule\OffenseDefinition")
     * @var OffenseDefinition
     */
    protected $offenseDefinition;

    /**
     * @Column(type="date", nullable=false)
     * @var DateTime
     */
    protected $date;

    /**
     * @Column(type="text", nullable=false)
     * @var string
     */
    protected $place;

    /**
     * @Column(type="integer", nullable=false)
     * @var int
     */
    protected $penaltyPoints;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Driver
     */
    public function getDriver()
    {
        return $this->driver;
    }

    public function setDriver(Driver $driver)
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * @return OffenseDefinition
     */
    public function getOffenseDefinition()
    {
        return $this->offenseDefinition;
    }

    public function setOffenseDefinition(OffenseDefinition $offenseDefinition)
    {
        $this->offenseDefinition = $offenseDefinition;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    public function setDate(DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

    public function getPlace()
    {
        return $this->place;
    }

    public function setPlace($place)
    {
        $this->place = $place;
        return $this;
    }

    public function getPenaltyPoints()
    {
        return $this->penaltyPoints;
    }

    public function setPenaltyPoints($penaltyPoints)
    {
        $this->penaltyPoints = $penaltyPoints;
        return $this;
    }

}
