<?php

namespace IISModule;

use Kdyby\Doctrine\QueryBuilder;
use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\EntityManager;
use Nette\Object;

/**
 * Abstract entity repository
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
abstract class BaseEntityManager extends Object
{

    /**
     *
     * @var EntityManager
     */
    private $em;

    /**
     *
     * @var type
     */
    private $entityName;

    /**
     * @param EntityManager $em
     * @param string $entityName
     */
    public function __construct(EntityManager $em, $entityName)
    {
        $this->em         = $em;
        $this->entityName = $entityName;
    }

    /**
     * Creates a new entity
     * @return BaseEntity
     */
    public function createEntity()
    {
        return new $this->entityName;
    }

    public function persist(BaseEntity $entity, $flush = false)
    {
        $this->em->persist($entity);

        if ($flush)
        {
            $this->em->flush($entity);
        }
    }

    public function flush()
    {
        $this->em->flush();
    }

    public function transactional($callback)
    {
        $this->getRepository()->transactional($callback);
    }

    public function delete($entity, $relations = null, $flush = false)
    {
        $this->getRepository()->delete($entity, $relations, $flush);
    }

    /**
     * @return EntityDao
     */
    public function getRepository()
    {
        return $this->em->getRepository($this->entityName);
    }

    /**
     *
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Finds a single entity by it's identifier
     *
     * @param mixed $id Entity identifier
     *
     * @return BaseEntity The entity instance or NULL if the entity can not be found.
     */
    public function findById($id)
    {
        return $this->findOneBy(array('id' => $id));
    }

    /**
     * Finds a single entity by a set of criteria.
     *
     * @param array $criteria
     * @param array|null $orderBy
     *
     * @return BaseEntity The entity instance or NULL if the entity can not be found.
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }

    /**
     * Finds entities by a set of criteria.
     *
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     *
     * @return array The objects.
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Fetches all records like $key => $value pairs
     *
     * @param array $criteria
     * @param string $value
     * @param string $key
     *
     * @throws \Exception|QueryException
     * @return array
     */
    public function findPairs($criteria, $value = NULL, $key = 'id')
    {
        return $this->getRepository()->findPairs($criteria, $value, $key);
    }

	/**
	 * @param string $alias
	 * @param string $indexBy The index for the from.
	 * @return \Kdyby\Doctrine\QueryBuilder
	 */
    public function createQueryBuilder($alias = NULL, $indexBy = NULL)
    {
        return $this->getRepository()->createQueryBuilder($alias, $indexBy);
    }

    /**
     * Create a new QueryBuilder instance that is pre-populated for this entity name
     *
     * @param string|NULL $alias
     * @param string|NULL $indexBy
     * @return \Kdyby\Doctrine\DqlSelection
     */
    public function select($alias = NULL, $indexBy = NULL)
    {
        return $this->getRepository()->select($alias, $indexBy);
    }

}
