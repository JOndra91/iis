<?php

namespace IISModule;

use ArrayAccess;
use Kdyby\Doctrine\Entities\BaseEntity as DoctrineBaseEntity;
use ReflectionProperty;

/**
 * BaseEntity
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
abstract class BaseEntity extends DoctrineBaseEntity
{

    /**
     * Transforms Entity into array
     * @return array
     */
    public function toArray()
    {
        $reflection = $this->getReflection();
        $properties = $reflection->getProperties(ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PUBLIC);

        $data = array();

        foreach ($properties as $property)
        {
            $propName = $property->name;
            if ($property->hasAnnotation("Column") && !is_null($method   = $reflection->getMethod('get' . $propName)))
            {
                $data[$propName] = $method->invoke($this);
            }
        }

        return $data;
    }

    /**
     * Set values from array
     * @param ArrayAccess $values
     */
    public function fromArray(ArrayAccess $values)
    {
        $reflection = $this->getReflection();
        $properties = $reflection->getProperties(ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PUBLIC);

        foreach ($properties as $property)
        {
            $propName = $property->name;
            if ($property->hasAnnotation("Column") && isset($values[$propName]) && !is_null($method   = $reflection->getMethod('set' . $propName)))
            {
                $method->invoke($this, $values[$propName]);
            }
        }
    }

}
