<?php

namespace IISModule;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;

/**
 * OffenseList model
 * @Entity
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class OffenseDefinition extends BaseEntity
{

    const ENTITY = 'IISModule\\OffenseDefinition';

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string", length=255, nullable=false)
     * @var string
     */
    protected $name;

    /**
     * @Column(type="text", nullable=false)
     * @var string
     */
    protected $description;

    /**
     * @Column(type="integer", nullable=false)
     * @var int
     */
    protected $penaltyPoints;

    /**
     * @Column(type="text", length=255, nullable=false)
     * @var string
     */
    protected $penalty;

    /**
     * @Column(type="boolean", options={"default"=1})
     * @var bool
     */
    protected $enabled = true;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getPenaltyPoints()
    {
        return $this->penaltyPoints;
    }

    public function setPenaltyPoints($penaltyPoints)
    {
        $this->penaltyPoints = $penaltyPoints;
        return $this;
    }

    public function getPenalty()
    {
        return $this->penalty;
    }

    public function setPenalty($penalty)
    {
        $this->penalty = $penalty;
        return $this;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled = true)
    {
        $this->enabled = $enabled;
        return $this;
    }

}
