<?php

namespace IISModule;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToOne;
use Nette\DateTime;

/**
 * LicensePlate model
 * @Entity
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class LicensePlate extends BaseEntity
{

    const ENTITY = 'IISModule\\LicensePlate';

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string", length=50, unique=true, nullable=false)
     * @var string
     */
    protected $code;

    /**
     * @Column(type="string", length=255, unique=false, nullable=false)
     * @var string
     */
    protected $region;

    /**
     * @Column(type="date", nullable=false)
     * @var DateTime
     */
    protected $assignDate;

    /**
     * @OneToOne(targetEntity="IISModule\Vehicle", inversedBy="licensePlate")
     * @JoinColumn(referencedColumnName="id")
     * @var Vehicle
     */
    protected $vehicle;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    public function addRegion($regions)
    {
        $this->region[] = (string) $regions;
    }

    /**
     * @return DateTime
     */
    public function getAssignDate()
    {
        return $this->assignDate;
    }

    public function setAssignDate(DateTime $assignDate)
    {
        $this->assignDate = $assignDate;
        return $this;
    }

    /**
     * @return Vehicle
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    public function setVehicle(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
        return $this;
    }

}
