<?php

namespace IISModule;

use Nette\Object;
use Nette\Security;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;

/**
 * Users authenticator.
 */
class Authenticator extends Object implements Security\IAuthenticator
{

    /**
     * Instance of user table
     * @var UserManager
     */
    private $userManager;

    public function __construct(UserManager $user)
    {
        $this->userManager = $user;
    }

    /**
     * Performs an authentication.
     * @return Identity
     * @throws AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;
        $user = $this->userManager->findByUsername($username);

        if (!$user)
        {
            throw new Security\AuthenticationException('Neplatné přihlašovací jméno.', self::IDENTITY_NOT_FOUND);
        }

        if ($user->getPassword() !== $this->calculateHash($password))
        {
            throw new Security\AuthenticationException('Špatné heslo.', self::INVALID_CREDENTIAL);
        }

        $arr = $user->toArray();
        unset($arr['password']);
        return new Identity($user->getId(), $user->getRole(), $arr);
    }

    /**
     * Computes salted password hash.
     * @param  string
     * @return string
     */
    public static function calculateHash($password)
    {
        return sha1('64&ab%!her$' . $password);
    }

}
