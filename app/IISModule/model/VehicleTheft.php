<?php

namespace IISModule;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Nette\DateTime;

/**
 * VehicleTheft
 * @Entity
 * @author Pavlína Bortlová <xbortl01@stud.fit.vutbr.cz>
 */
class VehicleTheft extends BaseEntity
{

    const ENTITY = 'IISModule\\VehicleTheft';

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="IISModule\Vehicle", inversedBy="vehicleThefts")
     * @var Vehicle
     */
    protected $vehicle;

    /**
     * @Column(type="date", nullable=false)
     * @var DateTime
     */
    protected $theftDate;

    /**
     * @Column(type="text", nullable=false)
     * @var string
     */
    protected $location;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Vehicle
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    public function setVehicle(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTheftDate()
    {
        return $this->theftDate;
    }

    public function setTheftDate(DateTime $theftDate)
    {
        $this->theftDate = $theftDate;
        return $this;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }



}
