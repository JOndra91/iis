<?php

namespace IISModule;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PostLoad;
use Doctrine\ORM\Mapping\PrePersist;
use Nette\DateTime;

/**
 * DriverLicense model
 * @Entity
 * @HasLifecycleCallbacks
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class DriverLicense extends BaseEntity
{

    const ENTITY = "IISModule\\DriverLicense";

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="IISModule\Driver", inversedBy="driverLicense")
     * @var Driver
     */
    protected $driver;

    /**
     * @Column(type="string", length=32, unique=true, nullable=false)
     * @var string
     */
    protected $code;

    /**
     * @Column(type="date", nullable=false)
     * @var DateTime
     */
    protected $issueDate;

    /**
     * @Column(type="date", nullable=false)
     * @var DateTime
     */
    protected $validUntil;

    /**
     * @Column(type="string", length=128, unique=false, nullable=false)
     * @var array
     */
    protected $categories;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Driver
     */
    public function getDriver()
    {
        return $this->driver;
    }

    public function setDriver(Driver $driver)
    {
        $this->driver = $driver;
        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    public function setIssueDate(DateTime $issueDate)
    {
        $this->issueDate = $issueDate;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    public function setValidUntil(DateTime $validUntil)
    {
        $this->validUntil = $validUntil;
        return $this;
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function setCategories(array $categories)
    {
        $this->categories = $categories;
        return $this;
    }

    public function addCategory($category)
    {
        $this->categories[] = (string) $category;
    }

    /**
     * @PostLoad
     */
    public function postLoad()
    {
        $this->categories = explode(' ', $this->categories);
        sort($this->categories);
    }

    /**
     * @PrePersist
     */
    public function prePersist()
    {
        $this->categories = implode(' ', $this->categories);
    }

}
