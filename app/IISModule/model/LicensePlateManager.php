<?php

namespace IISModule;

/**
 * LicensePlate manager
 *
 * @author Pavlína Bortlová <xbortl01@stud.fit.vutbr.cz>
 */
class LicensePlateManager extends BaseEntityManager
{
	public function licensePlateWithCodeExists($code)
    {

        $newSel = $this->select('1')
                ->from(LicensePlate::ENTITY, 'p')
                ->where('p.code = :code')
                ->limit(1);

        $newSel->setParameter('code', $code);
        $q = $newSel->createQuery();
        $result = $q->getResult();

        return !empty($result);
    }

}
