<?php

namespace IISModule;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Nette\DateTime;

/**
 * Vehicle
 * @Entity
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class Vehicle extends BaseEntity
{

    const ENTITY = 'IISModule\\Vehicle';

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="IISModule\Driver", inversedBy="vehicles")
     * @var Driver
     */
    protected $driver;

    /**
     * @OneToOne(targetEntity="LicensePlate", mappedBy="vehicle")
     * @var LicensePlate
     */
    protected $licensePlate;

    /**
     * @Column(type="string", length=32, unique=true, nullable=false)
     * @var string
     */
    protected $serialNumber;

    /**
     * @Column(type="string", length=128, nullable=false)
     * @var string
     */
    protected $manufacturer;

    /**
     * @Column(type="date", nullable=false)
     * @var DateTime
     */
    protected $dateOfManufacture;

    /**
     * @Column(type="string", length=128, nullable=false)
     * @var string
     */
    protected $model;

    /**
     * @Column(type="string", length=128, nullable=false)
     * @var string
     */
    protected $fuelType;

    /**
     * @Column(type="integer", nullable=false)
     * @var int
     */
    protected $weight;

    /**
     * @Column(type="integer", nullable=false)
     * @var int
     */
    protected $passengerCapacity;

    /**
     * @Column(type="integer", nullable=false)
     * @var int
     */
    protected $maxSpeed;

    /**
     * @OneToMany(targetEntity="IISModule\VehicleTheft", mappedBy="vehicle", cascade={"persist"}, orphanRemoval=true)
     * @var VehicleTheft[]
     */
    protected $vehicleThefts;

    /**
     * @OneToMany(targetEntity="IISModule\TechnicalCheck", mappedBy="vehicle", cascade={"persist"}, orphanRemoval=true)
     * @var TechnicalCheck[]
     */
    protected $technicalChecks;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Driver
     */
    public function getDriver()
    {
        return $this->driver;
    }

    public function setDriver(Driver $driver)
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * @return LicensePlate
     */
    public function getLicensePlate()
    {
        return $this->licensePlate;
    }

    public function setLicensePlate(LicensePlate $licensePlates)
    {
        $this->licensePlate = $licensePlates;
        return $this;
    }

    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;
        return $this;
    }

    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateOfManufacture()
    {
        return $this->dateOfManufacture;
    }

    public function setDateOfManufacture(DateTime $dateOfManufacture)
    {
        $this->dateOfManufacture = $dateOfManufacture;
        return $this;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function getFuelType()
    {
        return $this->fuelType;
    }

    public function setFuelType($fuelType)
    {
        $this->fuelType = $fuelType;
        return $this;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function getPassengerCapacity()
    {
        return $this->passengerCapacity;
    }

    public function setPassengerCapacity($passengerCapacity)
    {
        $this->passengerCapacity = $passengerCapacity;
        return $this;
    }

    public function getMaxSpeed()
    {
        return $this->maxSpeed;
    }

    public function setMaxSpeed($maxSpeed)
    {
        $this->maxSpeed = $maxSpeed;
        return $this;
    }

    /**
     * @return VehicleTheft[]
     */
    public function getVehicleThefts()
    {
        return $this->vehicleThefts;
    }

    /**
     * @param VehicleTheft[] $vehicleThefts
     * @return Vehicle
     */
    public function setVehicleThefts(array $vehicleThefts)
    {
        $this->vehicleThefts = $vehicleThefts;
        return $this;
    }

    /**
     * @param VehicleTheft $vehicleTheft
     * @return Vehicle
     */
    public function addVehicleTheft(VehicleTheft $vehicleTheft)
    {
        $this->vehicleThefts[] = $vehicleTheft;
        return $this;
    }

    /**
     * @return TechnicalCheck[]
     */
    public function getTechnicalChecks()
    {
        return $this->technicalChecks;
    }

    /**
     * @param TechnicalCheck[] $technicalChecks
     * @return \IISModule\Vehicle
     */
    public function setTechnicalChecks(array $technicalChecks)
    {
        $this->technicalChecks = $technicalChecks;
        return $this;
    }

    /**
     * @param \IISModule\TechnicalCheck $technicalCheck
     * @return \IISModule\Vehicle
     */
    public function addTechnicalCheck(TechnicalCheck $technicalCheck)
    {
        $this->technicalChecks[] = $technicalCheck;
        return $this;
    }

}
