<?php

namespace IISModule;

/**
 * DriverLicense repository
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class DriverLicenseManager extends BaseEntityManager
{

    public function licenseWithCodeExists($code)
    {

        $sel = $this->select('1')
                ->from(DriverLicense::ENTITY, 'l')
                ->where('l.code = :code')
                ->limit(1);

        $sel->setParameter('code', $code);
        $q = $sel->createQuery();
        $result = $q->getResult();

        return !empty($result);
    }

}
