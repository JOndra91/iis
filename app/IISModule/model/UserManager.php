<?php

namespace IISModule;

/**
 * User repository
 *
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class UserManager extends BaseEntityManager
{
    /**
     * Returns user with same username
     * @param string $username
     * @return User
     */
    public function findByUsername($username)
    {
        return $this->findOneBy(array('username' => $username));
    }

}
