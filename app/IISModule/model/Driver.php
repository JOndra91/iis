<?php

namespace IISModule;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PostLoad;
use Nette\DateTime;

/**
 * Driver model
 * @Entity
 * @HasLifecycleCallbacks
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */
class Driver extends BaseEntity
{

    const ENTITY = 'IISModule\\Driver';

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string", length=255, unique=false, nullable=false)
     * @var string
     */
    protected $name;

    /**
     * @Column(type="string", length=255, unique=false, nullable=false)
     * @var string
     */
    protected $surname;

    /**
     * @Column(type="string", length=32, unique=true, nullable=false)
     * @var string
     */
    protected $birthCode;

    /**
     * @Column(type="date", nullable=false)
     * @var DateTime
     */
    protected $birthDate;

    /**
     * @Column(type="date", nullable=false, options={"default"="0000-00-00"})
     * @var DateTime
     */
    protected $drivingRestricted = '0000-00-00';

    /**
     * @OneToMany(targetEntity="IISModule\DriverLicense", mappedBy="driver", cascade={"persist"}, orphanRemoval=true)
     * @var DriverLicense
     */
    protected $driverLicenses;

    /**
     * @OneToMany(targetEntity="IISModule\Offense", mappedBy="driver", cascade={"persist"}, orphanRemoval=true)
     * @var Offense[]
     */
    protected $offenses;

    /**
     * @OneToMany(targetEntity="IISModule\Vehicle", mappedBy="driver", cascade={"persist"}, orphanRemoval=true)
     * @var Vehicle[]
     */
    protected $vehicles;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return \IISModule\Driver
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return \IISModule\Driver
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return \IISModule\Driver
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    public function getBirthCode()
    {
        return $this->birthCode;
    }

    /**
     * @param string $birthCode
     * @return \IISModule\Driver
     */
    public function setBirthCode($birthCode)
    {
        $this->birthCode = $birthCode;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param DateTime $birthDate
     * @return \IISModule\Driver
     */
    public function setBirthDate(DateTime $birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    public function getDrivingRestricted()
    {
        return $this->drivingRestricted;
    }

    /**
     * @param DateTime $drivingRestricted
     * @return \IISModule\Driver
     */
    public function setDrivingRestricted(DateTime $drivingRestricted)
    {
        $this->drivingRestricted = $drivingRestricted;
        return $this;
    }

    /**
     * @return DriverLicense[]
     */
    public function getDriverLicenses()
    {
        return $this->driverLicenses;
    }

    /**
     * @param \IISModule\DriverLicense[] $driverLicenses
     * @return \IISModule\Driver
     */
    public function setDriverLicenses(array $driverLicenses)
    {
        $this->driverLicenses = $driverLicenses;
        return $this;
    }

    /**
     * @param \IISModule\DriverLicense $driverLicense
     * @return \IISModule\Driver
     */
    public function addDriverLicense(DriverLicense $driverLicense)
    {
        $this->driverLicenses[] = $driverLicense;
        return $this;
    }

    /**
     * @return Offense[]
     */
    public function getOffenses()
    {
        return $this->offenses;
    }

    /**
     * @param Offense[] $offenses
     * @return \IISModule\Driver
     */
    public function setOffenses(array $offenses)
    {
        $this->offenses = $offenses;
        return $this;
    }

    /**
     * @param \IISModule\Offense $offense
     * @return \IISModule\Driver
     */
    public function addOffense(Offense $offense)
    {
        $this->offenses[] = $offense;
        return $this;
    }

    /**
     * @return Vehicle[]
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }

    /**
     * @param Vehicle[] $vehicles
     * @return \IISModule\Driver
     */
    public function setVehicles(array $vehicles)
    {
        $this->vehicles = $vehicles;
        return $this;
    }

    /**
     * @param \IISModule\Vehicle $vehicle
     * @return \IISModule\Driver
     */
    public function addVehicle(Vehicle $vehicle)
    {
        $this->vehicles[] = $vehicle;
        return $this;
    }

    /**
     * @PrePersist
     * @PostLoad
     */
    public function prePersist()
    {
        $today = new DateTime();

        if ($this->drivingRestricted < $today)
        {
            $this->drivingRestricted = new DateTime('0000-00-00');
        }
    }

}
