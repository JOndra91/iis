Projekt IIS - Registr řidičů a vozidel
======================================

Instalace
---------

1. Nainstalovat composer: (více na http://getcomposer.org/download)

		curl -s http://getcomposer.org/installer | php

2. Získat závislosti:

		php composer.phar install

Adresáře `temp` a `log` zpřístupnit pro zápis.

Vytvoření databáze
------------------

1. Vytvořit soubor `app/config/config.local.neon`

2. Vložit a doplnit tento kód

        doctrine:
            user: <your username>
            password: <your password>

        console:
            url: <your url>

3. Z adresáře projektu spustit příkaz

        app/console orm:schema-tool:create

Úkoly
-----

### Ondřej Janošík

1. Správa řidičů
1. Správa přestupků
1. Správa řidičských průkazů

### Pavlína Bortlová

1. Správa vozidel
    - Do gridu vložit subgridy
        - s řidičem
        - s krádežemi
        - s technickými kontrolami
1. Správa krádeží
1. Správa SPZ
    - Je třeba aby bylo možné vkládat SPZ hromadně, nebo třeba i generovat
1. Technické kontroly
    - Zde bude třeba vytvořit novou roli pro technika
    - Technická kontrola se bude přidávat přes přehled vozidel pomocí akce "Provést kontrolu" (pojmenování je na tobě)

